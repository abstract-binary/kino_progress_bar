{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = github:edolstra/flake-compat;
      flake = false;
    };
    nextls = {
      url = "github:elixir-tools/next-ls";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, nextls, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShell = with pkgs; mkShell {
          buildInputs = [
            difftastic
            elixir
            erlang
            flyctl
            inotify-tools
            just
            postgresql_15
            pre-commit
            tokei
            nextls.packages."${system}".default
            # nodejs-18_x
          ];
          GIT_EXTERNAL_DIFF = "${difftastic}/bin/difft";
        };
      });
}
