# KinoProgressBar

> Progress bar widget for [Livebook](https://livebook.dev)

## Usage

Create a new progress bar with `KinoProgressBar.new/2` and update it
with `KinoProgressBar.set_current/2`.

![Screenshot of KinoProgressBar in action](doc_img/example1.jpg)

Features:

- configurable range,
- configurable color,
- throttled client updates

See [the
docs](https://hexdocs.pm/kino_progress_bar/KinoProgressBar.html) for
more details.

## Installation

Add the following to the dependencies section of your notebook:

```elixir
Mix.install([
  {:kino_progress_bar, "~> 0.1.0"}
])
```
