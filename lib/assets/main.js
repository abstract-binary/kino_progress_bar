export function init(ctx, data) {
    ctx.importCSS("main.css");
    ctx.root.innerHTML = `
      <span>${data.title}</span>
      <div id="kino-progress-bar" style="width: ${data.width}px;">
        <div id="kino-progress-bar-color" style="width: ${data.current_px}px; background: ${data.color}"></div>
        <div id="kino-progress-bar-text" style="width: ${data.width}px;">${data.current_text}</div>
      </div>
    `;

    const colorBar = ctx.root.querySelector("#kino-progress-bar-color");
    const textBar = ctx.root.querySelector("#kino-progress-bar-text");

    ctx.handleEvent("set_current", (data) => {
        colorBar.style.width = `${data.current_px}px`;
        textBar.innerHTML = `${data.current_text}`;
    });
}
