defmodule KinoProgressBarTest do
  use ExUnit.Case
  doctest KinoProgressBar

  import Kino.Test

  setup :configure_livebook_bridge

  test "set_current" do
    title = "Example"
    pb1 = KinoProgressBar.new(title)
    assert %{title: ^title} = connect(pb1)
    :ok = KinoProgressBar.set_current(pb1, 20)

    assert_broadcast_event(
      pb1,
      "set_current",
      %{current_px: 120, current_text: "20%"},
      1000
    )

    # Sending an out-of-range value just goes to the end of the bar
    # instead of overflowing, but displays the requested string.
    :ok = KinoProgressBar.set_current(pb1, 120)

    assert_broadcast_event(
      pb1,
      "set_current",
      %{current_px: 600, current_text: "120%"},
      1000
    )
  end

  test "custom_current" do
    title = "Example"
    pb1 = KinoProgressBar.new(title, format_current: &"Magic: #{&1}¢")
    assert %{title: ^title} = connect(pb1)
    :ok = KinoProgressBar.set_current(pb1, 20)

    assert_broadcast_event(
      pb1,
      "set_current",
      %{current_px: 120, current_text: "Magic: 20¢"},
      1000
    )
  end
end
